package main

import (
	_ "gitlab.com/g3n/g3nd/demos/animation"
	_ "gitlab.com/g3n/g3nd/demos/audio"
	_ "gitlab.com/g3n/g3nd/demos/experimental/physics"
	_ "gitlab.com/g3n/g3nd/demos/geometry"
	_ "gitlab.com/g3n/g3nd/demos/gui"
	_ "gitlab.com/g3n/g3nd/demos/helper"
	_ "gitlab.com/g3n/g3nd/demos/light"
	_ "gitlab.com/g3n/g3nd/demos/loader"
	_ "gitlab.com/g3n/g3nd/demos/material"
	_ "gitlab.com/g3n/g3nd/demos/other"
	_ "gitlab.com/g3n/g3nd/demos/shader"
	_ "gitlab.com/g3n/g3nd/demos/tests"
	_ "gitlab.com/g3n/g3nd/demos/texture"

	"gitlab.com/g3n/g3nd/app"
)

func main() {

	// Create and run application
	app.Create().Run()
}
